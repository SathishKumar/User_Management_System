#1 Importing section

from flask import Flask, Response, redirect, url_for, request, session, render_template, abort, make_response
from flask.ext.login import LoginManager, UserMixin, \
				login_required, login_user, logout_user
import os

from flask.ext.sqlalchemy import SQLAlchemy

#2 configuring section
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['DATABASE_FILE'] = 'user_db.sqlite'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + app.config['DATABASE_FILE']
app.secret_key = '12345'


db = SQLAlchemy(app)
db.init_app(app)


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "home" #this defines which method need to call if user is accesing user_profile or loging out method through url without loggin in


#3 Data base modeling section
class User(db.Model):

   # __tablename__ = 'user'
    
    fname = db.Column(db.String)
    lname = db.Column(db.String)
    gender = db.Column(db.String)
    dept = db.Column(db.String)
    phone = db.Column(db.String)
    dist = db.Column(db.String)
    uname = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)
    authenticate = db.Column(db.Boolean, default=False)

    def __init__(self, fname, lname, gender, dept, phone, dist, uname, password, authenticate):
        self.fname = fname
        self.lname = lname
        self.gender = gender
        self.dept = dept
        self.phone = phone
        self.dist = dist
        self.uname = uname
        self.password = password
        self.authenticate = authenticate

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.uname

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False
   
    def __repr__(self):
        return '<User %r>' % (self.uname)

db.create_all() #creating the table with all feilds
db.session.commit() #commiting the database

#4 User Loading Section
@login_manager.user_loader
def user_loader(uname):
        return User.query.get(uname)


#5 Launching section    
@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template("index.html")

#6 Signup section
@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        return render_template("signup.html")
    
    if request.method == 'POST':
        fname = request.form['fname']
        lname = request.form['lname']
        gender = request.form['sex']
        dept = request.form['dept']
        uname = request.form['email']
        phone = request.form['phone']
        dist = request.form['dist']
        password = request.form['password']
        authenticate = False
        user = User.query.filter_by(uname=uname).first()
        if user:
            msg = "User Already Registered"
            return render_template("login.html", info=msg)
        datas = User(fname, lname, gender, dept, phone, dist, uname, password, authenticate)
        db.session.add(datas)
        db.session.commit()
        done = "Registerd Successfully! Login to continue"
        return render_template("login.html", done=done)

#7 login section
@app.route('/login', methods = ['GET', 'POST'])
def getting_user():
    if request.method == 'GET':
        return render_template("login.html")
    if request.method == 'POST':
        uname = request.form['email']
        password = request.form['password']
        user = User.query.filter_by(uname=uname,password=password).first()
        if user is None:
            msg = "Invalid user name or password"
            return render_template("login.html", msg=msg)
        login_user(user)
        
        user.authenticate = True
        db.session.add(user)
        db.session.commit()
        name = user.fname + " " + user.lname
        resp = make_response(render_template('hello.html', name=name))
        Response.set_cookie(resp,'uname',uname)
        return resp

#8 profile section
@app.route('/user_profile', methods = ['GET'])
@login_required #this prevents this method from the user not logged in
def profile():
    uname = request.cookies.get('uname')
    user = User.query.filter_by(uname=uname).first()
    name = user.fname + " " + user.lname
    gender = user.gender
    dept = user.dept
    phone = user.phone
    dist = user.dist
    return render_template("userprofile.html", name=name, gender=gender, dept=dept, phone=phone, dist=dist)

#9 logout section
@app.route('/logout', methods = ['GET'])
@login_required
def logout():
    uname = request.cookies.get('uname')
    user = User.query.filter_by(uname=uname).first()
    user.authenticate = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return render_template("index.html")

#10 User details section
@app.route('/change_name', methods = ['GET','POST'])
@login_required 
def change_name():
    if request.method == 'GET':
        return redirect('/user_profile')
    if request.method == 'POST':
        uname = request.cookies.get('uname')
        user = User.query.filter_by(uname=uname).first()
        fname = request.form['fname']
        lname = request.form['lname']
        user.fname = fname
        user.lname = lname
        db.session.add(user)
        db.session.commit()
        return redirect('/user_profile')

@app.route('/change_gender', methods = ['GET','POST'])
@login_required 
def change_gender():
    if request.method == 'GET':
        return redirect('/user_profile')
    if request.method == 'POST':
        uname = request.cookies.get('uname')
        user = User.query.filter_by(uname=uname).first()
        gender = request.form['gender']
        user.gender = gender
        db.session.add(user)
        db.session.commit()
        return redirect('/user_profile')

@app.route('/change_dept', methods = ['GET','POST'])
@login_required 
def change_dept():
    if request.method == 'GET':
        return redirect('/user_profile')
    if request.method == 'POST':
        uname = request.cookies.get('uname')
        user = User.query.filter_by(uname=uname).first()
        dept = request.form['dept']
        user.dept = dept
        db.session.add(user)
        db.session.commit()
        return redirect('/user_profile')

@app.route('/change_phone', methods = ['GET','POST'])
@login_required 
def change_phone():
    if request.method == 'GET':
        return redirect('/user_profile')
    if request.method == 'POST':
        uname = request.cookies.get('uname')
        user = User.query.filter_by(uname=uname).first()
        phone = request.form['phone']
        user.phone = phone
        db.session.add(user)
        db.session.commit()
        return redirect('/user_profile')

@app.route('/change_dist', methods = ['GET','POST'])
@login_required 
def change_dist():
    if request.method == 'GET':
        return redirect('/user_profile')
    if request.method == 'POST':
        uname = request.cookies.get('uname')
        user = User.query.filter_by(uname=uname).first()
        dist = request.form['dist']
        user.dist = dist
        db.session.add(user)
        db.session.commit()
        return redirect('/user_profile')


##You can add your own module here

if __name__ == '__main__':
      app.run(host='localhost', port=8000)

