from flask import Flask, Response, redirect, url_for, request, session, render_template, abort
from flask.ext.login import LoginManager, UserMixin, \
				login_required, login_user, logout_user
import os

from flask.ext.sqlalchemy import SQLAlchemy
import sqlite3


app = Flask(__name__)
#configuration
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['DATABASE_FILE'] = 'admin.sqlite'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + app.config['DATABASE_FILE']
app.secret_key = 'sathish'
db = SQLAlchemy(app)
db.init_app(app)

#login configuration
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "home"


#creating admin class
class Admin_user(db.Model):
    aname = db.Column(db.String, primary_key=True)
    apass = db.Column(db.String)
    ath = db.Column(db.Boolean, default=False)
    def __init__(self, aname, apass, ath):
        self.aname = aname
        self.apass = apass
        self.ath = ath
    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.aname

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.ath

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False
   
    def __repr__(self):
        return '<User %r>' % (self.aname)

#forms the database along with table
db.create_all()
db.session.commit()


aname = "sathishkumar"
apass = "sathish4043"
ath = False
datas = Admin_user(aname, apass,  ath)
db.session.add(datas)

@login_manager.user_loader
def user_loader(aname):
        return Admin_user.query.get(aname)



@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        return render_template("admin_login.html")
    if request.method == 'POST':
        aname = request.form['aname']
        apass = request.form['password']
        print aname, apass
        admin = Admin_user.query.filter_by(aname=aname, apass=apass).first()
        if admin is None:
            msg = "invalid admin"
            print admin
            return render_template("admin_login.html", msg=msg)
        login_user(admin)
        admin.ath = True
        db.session.add(admin)
        db.session.commit()
        return render_template("adminpanel.html")

@app.route('/getusers', methods=['GET'])
@login_required
def getusers():
    connection = sqlite3.connect("user_db.sqlite")
    cursor = connection.cursor()
    select_query = "SELECT * FROM user"
    cursor.execute(select_query)
    data = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()
    users = []
    for i in data:
        users.append(i)
    return render_template("userpage.html", users=users)

@app.route('/logout', methods = ['GET'])
@login_required
def logout():

    logout_user()
    return render_template("adminpanel.html")

#deleting the user
@app.route('/delete_user', methods = ['POST'])
@login_required
def del_user():
    uname = request.form['uname']
    print uname
    connection = sqlite3.connect("user_db.sqlite")
    cursor = connection.cursor()
    delete_user = "DELETE FROM user WHERE uname = '%s'" % (uname)
    cursor.execute(delete_user)
    connection.commit()
    cursor.close()
    connection.close()
    return redirect("/getusers")

app.run(debug=True)


